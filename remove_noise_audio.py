import os
import argparse

def remove_noise(input_dir, output_dir, script_dir, gpu, sample_rate):
    # Create the output directory if it does not exist
    os.makedirs(output_dir, exist_ok=True)

    # Loop through all audio files in the input directory
    for filename in os.listdir(input_dir):
        if filename.endswith(".wav"):
            audio_file_path = os.path.join(input_dir, filename)

            print(f"Start processing: {filename}")
            # Run the command for each audio file
            command = f"cd {script_dir} && ./run_effect.sh -g {gpu} -s {sample_rate} -o {sample_rate} -b 1 -v -e dereverb_denoiser -i \"{audio_file_path}\" "
            move_out = f"mv {script_dir}/dereverb_denoiser/48k/\"{filename}\" \"{output_dir}\" "
            # Capture the output of the command and display it
            os.system(command)
            os.system(move_out)
            print(f"Processed: {filename}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Remove noise from audio files")
    parser.add_argument("--audio-input-dir", type=str, default="/content/audio_output", help="Input directory for audio files")
    parser.add_argument("--output-dir", type=str, default="/content/drive/MyDrive/audio_output", help="Output directory for processed audio files")
    parser.add_argument("--script-dir", type=str, default="/content/Audio_Effects_SDK/samples/effects_demo", help="Script directory for processed audio files")
    parser.add_argument("--gpu", type=str, default="t4", choices=["v100","t4","a10","a100"], help="gpu type")
    parser.add_argument("--sample_rate", type=int, default=48, choices=[16,48], help="Sample rate")
	
    args = parser.parse_args()

    remove_noise(args.audio_input_dir, args.output_dir, args.script_dir, args.gpu, args.sample_rate)

#python remove_noise_audio.py --audio-input-dir /path/to/audio/files --output-dir /path/to/output
