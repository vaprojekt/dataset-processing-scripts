import os
import argparse

def transcribe_audio(input_dir, output_dir, language, script_dir, min_speakers):
    os.makedirs(output_dir, exist_ok=True)

    # Loop through all audio files in the input directory
    for filename in os.listdir(input_dir):
        if filename.endswith(".mp3") or filename.endswith(".wav"):
            audio_file_path = os.path.join(input_dir, filename)
            json_output_path = os.path.join(output_dir, f"{filename}-result.json")
			
            print(f"Start processing: {filename}")
            # Run the command for each audio file
            # command = f"cd whisper-webui/ && python cli.py --whisper_implementation 'faster-whisper' --task transcribe --auto_parallel True --model large-v2 --vad silero-vad --language {language} --word_timestamps False \"{audio_file_path}\" --output_dir \"{output_dir}\""
            command = f"python {script_dir}/cli.py --whisper_implementation faster-whisper --task transcribe --auto_parallel True --model large-v2 --vad silero-vad --language {language} --word_timestamps False \"{audio_file_path}\" --output_dir \"{output_dir}\""
            
            # Capture the output of the command and display it
            os.system(command)
            
            command = f"python {script_dir}/whisper-diarization/app.py --auth_token huggingface_api_key --min_speakers {min_speakers} \"{audio_file_path}\" \"{json_output_path}\""
            # Capture the output of the command and display it
            os.system(command)
            print(f"Processed: {filename}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Transcribe audio files using Whisper")
    parser.add_argument("--audio-input-dir", type=str, default="/content/drive/MyDrive/audio_input", help="Input directory for audio files")
    parser.add_argument("--output-dir", type=str, default="/content/drive/MyDrive/Whisper/output", help="Output directory for transcribed text")
    # parser.add_argument("--language", type=str, choices=['Japanese', 'English', 'German'], default='German', help="Language for transcription")
    parser.add_argument("--language", type=str, default='German', help="Language for transcription")
    parser.add_argument("--script-dir", type=str, default="/content/drive/MyDrive/audio_input", help="Whisper-webui cli script directory")
    parser.add_argument("--min-speakers", type=int, default=10, help="Minimal Number of speaker to extract")
	
    args = parser.parse_args()

    transcribe_audio(args.audio_input_dir, args.output_dir, args.language, args.script_dir, args.min_speakers)

#python speaker_diarization_extracted_json_only.py --audio-input-dir /path/to/audio/files --output-dir /path/to/output --language German --script-dir /path/to/script-dir(whisper-webui)
