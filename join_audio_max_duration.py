import argparse
import random
from pydub import AudioSegment
import os
import re

def extract_index(filename):
    match = re.search(r"\d+", filename)
    if match:
        return int(match.group())
    return -1

def join_audio(input_dir, output_dir, output_prefix, playback_speed=1.0, max_part_duration=240000, add_silent_space=True, enable_export_transcript=False):
    # Get a list of all audio files in the input directory
    audio_files = sorted([file for file in os.listdir(input_dir) if file.endswith(".wav")],
                         key=extract_index)

    # Initialize variables to keep track of joined audio and part duration
    joined_audio = AudioSegment.empty()
    part_duration = 0
    part_number = 1

    # Initialize variables for transcript combination
    part_transcript = ""
    # Iterate over the audio files and load them as audio segments
    for audio_file in audio_files:
        audio_path = os.path.join(input_dir, audio_file)
        segment = AudioSegment.from_file(audio_path, format="wav")
        segment_duration = len(segment)

        # Load the corresponding transcript file if enabled
        if enable_export_transcript:
            transcript_path = os.path.join(input_dir, os.path.splitext(audio_file)[0] + ".txt")
            with open(transcript_path, "r", encoding="utf-8") as transcript_file:
                transcript_info = transcript_file.readline().strip().split("|")
                segment_transcript = transcript_info[1]
        
        # Check if adding the segment would exceed the maximum part duration
        if part_duration + segment_duration <= max_part_duration:
            joined_audio += segment
            part_duration += segment_duration
            
            # Combine the transcript if enabled and update the part transcript
            if enable_export_transcript:
                part_transcript += segment_transcript + " "

            # Add silent space between audio files if enabled
            if add_silent_space:
                remaining_space = max_part_duration - part_duration
                silent_duration = random.randint(300, min(600, remaining_space))
                silent_segment = AudioSegment.silent(duration=silent_duration)
                joined_audio += silent_segment
                part_duration += silent_duration
        else:
            # Export the joined audio as a part file
            part_output_file = os.path.join(output_dir, f"{output_prefix}_part_{part_number}.wav")
            adjusted_audio = joined_audio.speedup(playback_speed) if playback_speed != 1.0 else joined_audio
            adjusted_audio.export(part_output_file, format="wav")
            
            # Export the combined transcript if enabled
            if enable_export_transcript:
                with open(os.path.join(output_dir, f"{output_prefix}_part_{part_number}.txt"), "w", encoding="utf-8") as transcript_file:
                    transcript_file.write(f"{output_prefix}_part_{part_number}.wav|{part_transcript.strip()}|{part_duration/1000}")
            
            # Reset variables for the next part
            joined_audio = segment
            part_duration = segment_duration
            part_number += 1

            # Reset variables for the next transcript combination if enabled
            if enable_export_transcript:
                part_transcript = segment_transcript + " "
                
    # Export the last part
    last_part_output_file = os.path.join(output_dir, f"{output_prefix}_part_{part_number}.wav")
    adjusted_audio = joined_audio.speedup(playback_speed) if playback_speed != 1.0 else joined_audio
    adjusted_audio.export(last_part_output_file, format="wav")
    
    # Export the combined transcript for the last part if enabled
    if enable_export_transcript:
        with open(os.path.join(output_dir, f"{output_prefix}_part_{part_number}.txt"), "w", encoding="utf-8") as transcript_file:
            transcript_file.write(f"{output_prefix}_part_{part_number}.wav|{part_transcript.strip()}|{part_duration/1000}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Join WAV audio files into long audio parts")
    parser.add_argument("--input_dir", type=str, default='audio_files/', help="Path to the directory containing the audio files")
    parser.add_argument("--output_dir", type=str, default='output_parts/', help="Path to the output directory for parts")
    parser.add_argument("--output_prefix", type=str, default='output_audio', help="Prefix for the output audio part filenames")
    parser.add_argument("--max_part_duration", type=int, default=240000, help="Maximum duration of each audio part in milliseconds (default: 4 minutes)")
    parser.add_argument("--playback_speed", type=float, default=1.0,
                        help="Playback speed of the parts. Use 1.0 for the original speed.")
    parser.add_argument("--no_silent_space", action="store_true", help="Disable adding silent space between audio files")
    parser.add_argument("--enable_export_transcript", action="store_true", help="Enable exporting transcripts for each part")
    args = parser.parse_args()

    os.makedirs(args.output_dir, exist_ok=True)
    join_audio(args.input_dir, args.output_dir, args.output_prefix, args.playback_speed, args.max_part_duration, add_silent_space=not args.no_silent_space, enable_export_transcript=args.enable_export_transcript)

# Example usage:
# python join_audio_max_duration.py --input_dir /path/to/input/audio_files --output_prefix name_of_dataset --no_silent_space --enable_export_transcript
