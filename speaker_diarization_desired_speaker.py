from pydub import AudioSegment

import argparse
import json

def exclude_speakers_old(json_file, excluded_speakers):
    with open(json_file, 'r', encoding="utf-8") as f:
        data = json.load(f)
    
    excluded_segments = []
    for segment in data['segments']:
        if any(speaker['speaker'] in excluded_speakers for speaker in segment['speakers']):
            excluded_segments.append(segment)
    
    final_segments = [segment for segment in data['segments'] if segment not in excluded_segments]
    
    return final_segments

def exclude_speakers(json_file, excluded_speakers):
    with open(json_file, 'r', encoding="utf-8") as f:
        data = json.load(f)
    
    excluded_segments = []
    for segment in data['segments']:
        try:
            for speaker in segment['speakers']:
                if speaker['speaker'] in excluded_speakers:
                    excluded_segments.append(segment)
                    break  # Exit the loop once an excluded speaker is found in the segment
        except KeyError:
            # Handle the case when the 'speakers' key is not present in the segment
            pass

    final_segments = [segment for segment in data['segments'] if segment not in excluded_segments]
    
    return final_segments

def format_speaker_number(speaker):
    # Format the speaker number with leading zeros
    speaker_number = int(speaker)
    formatted_speaker = f"SPEAKER_{speaker_number:02d}"
    return formatted_speaker

def export_final_audio(input_file, final_segments, output_file):
    input_audio = AudioSegment.from_file(input_file)
    final_audio = AudioSegment.empty()

    for segment in final_segments:
        start_ms = int(segment['start'] * 1000)
        end_ms = int(segment['end'] * 1000)
        final_audio += input_audio[start_ms:end_ms]
    
    final_audio.export(output_file, format="wav")

def main():
    parser = argparse.ArgumentParser(description='Exclude desired speakers from final audio')
    parser.add_argument('--input_file', type=str, required=True, help="Path to the input directory containing audio files")
    parser.add_argument('--json_file', type=str, help='Path to the JSON file containing audio segments and speaker information')
    parser.add_argument('--excluded_speakers', type=str, help='List of speaker IDs to exclude')
    parser.add_argument('--output_file', type=str, default='final_audio.wav', help='Path to output final audio file')

    args = parser.parse_args()
    
    speakers_to_process = []
    if args.excluded_speakers:
        speakers_to_process = [format_speaker_number(speaker) for speaker in args.excluded_speakers.split(',')]

    final_segments = exclude_speakers(args.json_file, speakers_to_process)
    export_final_audio(args.input_file, final_segments, args.output_file)

if __name__ == "__main__":
    main()

# python "E:\tools\speaker_diarization_desired_speaker.py" --input_dir "C:\Users\Z820\Desktop\LSTQNNN_final" --json_dir "C:\Users\Z820\Desktop\LSTQNNN_final\output" --output_dir "C:\Users\Z820\Desktop\LSTQNNN_final\speaker_output" --excluded_speakers 0,3