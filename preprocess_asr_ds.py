import re
import json
import argparse
from tqdm import tqdm

def compute_char_counts(manifest):
    char_counts = {}
    with open(manifest, 'r') as fn_in:
        for line in tqdm(fn_in, desc="Compute counts.."):
            line = line.replace("\n", "")
            data = json.loads(line)
            text = data["text"]
            for word in text.split():
                for char in word:
                    if char not in char_counts:
                        char_counts[char] = 1
                    else:
                        char_counts[char] += 1
    return char_counts

def clear_data_set(manifest, char_rate_threshold=None):
    chars_to_ignore_regex = "[\.\,\?\:\-!;()«»…\]\[/\*–‽+&_\\½√>€™$•¼}{~—=“\"”″‟„]"
    #chars_to_ignore_regex = r'[\.,?\:\-!;()«»…\]\[/\*–‽+&_\\½√>€™$•¼}{~—=“"”″‟„%\'�]'
    addition_ignore_regex = f"[{''.join(trash_char_list)}]"

    manifest_clean = manifest + '.clean'
    war_count = 0
    with open(manifest, 'r') as fn_in, \
            open(manifest_clean, 'w', encoding='utf-8') as fn_out:
        for line in tqdm(fn_in, desc="Cleaning manifest data"):
            line = line.replace("\n", "")
            data = json.loads(line)
            text = data["text"]
            if char_rate_threshold and len(text.replace(' ', '')) / float(data['duration']) > char_rate_threshold:
                print(f"[WARNING]: {data['audio_filepath']} has char rate > 15 per sec: {len(text)} chars, {data['duration']} duration")
                war_count += 1
                continue
            text = re.sub(chars_to_ignore_regex, "", text)
            text = re.sub(addition_ignore_regex, "", text)
            data["text"] = text.lower()
            data = json.dumps(data, ensure_ascii=False)
            fn_out.write(f"{data}\n")
    print(f"[INFO]: {war_count} files were removed from manifest")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Clean manifest data")
    parser.add_argument("--data_root", type=str, required=True, help="Root directory for data")
    args = parser.parse_args()

    dev_manifest = f"{args.data_root}/validation/validation_mozilla-foundation_common_voice_11_0_manifest.json"
    test_manifest = f"{args.data_root}/test/test_mozilla-foundation_common_voice_11_0_manifest.json"
    train_manifest = f"{args.data_root}/train/train_mozilla-foundation_common_voice_11_0_manifest.json"

    char_counts = compute_char_counts(train_manifest)

    threshold = 10
    trash_char_list = []

    for char in char_counts:
        if char_counts[char] <= threshold:
            trash_char_list.append(char)

    print(trash_char_list)

    clear_data_set(dev_manifest)
    clear_data_set(test_manifest)
    clear_data_set(train_manifest, char_rate_threshold=15)

