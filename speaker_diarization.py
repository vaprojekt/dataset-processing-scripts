import argparse
from pydub import AudioSegment
import json
import os

def convert_to_wav(input_path):
    # Check if the file is not already in WAV format
    if not input_path.lower().endswith(".wav"):
        audio = AudioSegment.from_file(input_path)
        # Convert to WAV and save with the same filename but with .wav extension
        output_path = os.path.splitext(input_path)[0] + ".wav"
        audio.export(output_path, format="wav")
        return output_path
    return input_path

def format_speaker_number(speaker):
    # Format the speaker number with leading zeros
    speaker_number = int(speaker)
    formatted_speaker = f"SPEAKER_{speaker_number:02d}"
    return formatted_speaker

def split_audio(input_path, output_path, segments, speakers_to_process, sample_rate, bit_depth, ignore_text):
    audio = AudioSegment.from_wav(input_path)
    for segment in segments:
        transcript = segment.get("text", "")
        if ignore_text and ignore_text in transcript:
            continue  # Skip segments with the specified text
        
        speaker = segment.get("longest_speaker", None)
        if not speakers_to_process or speaker in speakers_to_process:
            start_time = int(segment["start"] * 1000)  # Convert to milliseconds
            end_time = int(segment["end"] * 1000)  # Convert to milliseconds
            part = audio[start_time:end_time]

            if not os.path.exists(output_path):
                os.makedirs(output_path)
		
            output_filename = f"part_{start_time}_{end_time}_{speaker}.wav"
            output_file = os.path.join(output_path, output_filename)

            part = part.set_frame_rate(sample_rate)
            part = part.set_sample_width(bit_depth // 8)

            part.export(output_file, format="wav")

            transcript_filename = os.path.splitext(output_filename)[0] + ".txt"
            transcript_path = os.path.join(output_path, transcript_filename)
            duration = (end_time - start_time) / 1000
            with open(transcript_path, "w", encoding="utf-8") as transcript_file:
                transcript_file.write(f"{output_filename}|{transcript}|gunter|other|{duration}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Split audio based on segments and speakers from JSON file.")
    parser.add_argument("--input_dir", type=str, required=True, help="Path to the input directory containing audio files")
    parser.add_argument("--json_dir", type=str, required=True, help="Path to the directory containing JSON files")
    parser.add_argument("--output_dir", type=str, required=True, help="Path to the output directory")
    parser.add_argument("--speakers", type=str, help="Comma-separated list of speakers to process")
    parser.add_argument("--sample_rate", type=int, default=22050, help="Desired sample rate for output audio")
    parser.add_argument("--bit_depth", type=int, default=16, choices=[16, 24, 32], help="Desired bit depth for output audio")
    parser.add_argument("--ignore_text", type=str, default="test text", help="Text to ignore segments containing this text")
    args = parser.parse_args()

    input_dir = args.input_dir
    json_dir = args.json_dir
    output_dir = args.output_dir
	
    speakers_to_process = []
    if args.speakers:
        speakers_to_process = [format_speaker_number(speaker) for speaker in args.speakers.split(',')]

    for filename in os.listdir(input_dir):
        if filename.lower().endswith(".mp3") or filename.lower().endswith(".wav"):
            input_file_path = os.path.join(input_dir, filename)
            input_file_path = convert_to_wav(input_file_path)  # Convert MP3 to WAV if needed
            #json_filename = f"{os.path.splitext(filename)[0]}-result_output.json"
            json_filename = f"{filename}-result_output.json"
            json_file_path = os.path.join(json_dir, json_filename)
			
            if os.path.exists(json_file_path):
                with open(json_file_path, "r", encoding="utf-8") as json_file:
                    data = json.load(json_file)
                    segments = data["segments"]
                    output_directory = os.path.join(output_dir, os.path.splitext(filename)[0])
                    split_audio(input_file_path, output_directory, segments, speakers_to_process, args.sample_rate, args.bit_depth, args.ignore_text)

# Example usage:
#python "\path\to\speaker_diarization.py" --input_dir "\path\to\audio.mp3" --json_dir "\path\to\audio.mp3-result_output.json"  --output_dir speaker_output --speakers 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 --arg_name "test text"
