import os
import argparse

def rename_files(input_dir):
    # List all files in the input directory
    files = os.listdir(input_dir)

    # Filter out non-audio files (if needed)
    audio_files = [file for file in files if file.endswith('.wav')]

    # Sort audio files numerically to ensure proper ordering
    audio_files.sort(key=lambda x: int(x.split('_')[-2].split('.')[0]))

    # Calculate the number of digits needed for numbering the files
    num_files = len(audio_files)
    num_digits = len(str(num_files))

    # Rename each audio file
    for index, file in enumerate(audio_files, start=1):
        # Print file name for debugging
        print("Processing file:", file)
        
        # Generate new file name
        new_name = f"file_{index:0{num_digits}d}.wav"

        # Construct old and new file paths
        old_path = os.path.join(input_dir, file)
        new_path = os.path.join(input_dir, new_name)

        # Rename the file
        os.rename(old_path, new_path)

        print(f"Renamed {file} to {new_name}")

if __name__ == "__main__":
    # Parse command-line arguments
    parser = argparse.ArgumentParser(description="Rename audio files in a directory")
    parser.add_argument("--input_dir", type=str, required=True, help="Path to the directory containing audio files")
    args = parser.parse_args()

    # Rename files in the specified directory
    rename_files(args.input_dir)

# python "E:\Custom_Voice_Training\deep_clone_voice\tools\nemo\rename_files_splitted_from_srt.py" --input_dir "C:\Users\Z820\Desktop\test\"