from pydub import AudioSegment

import argparse
import json

def exclude_speakers(json_file, excluded_speakers):
    with open(json_file, 'r', encoding="utf-8") as f:
        data = json.load(f)
    
    excluded_segments = []
    for segment in data['segments']:
        for speaker in segment['speakers']:
            print(f"Speaker in segment: {speaker['speaker']}")
            if speaker['speaker'] in excluded_speakers:
                print("Excluded speaker found!")
                excluded_segments.append(segment)
                break  # Exit the loop once an excluded speaker is found in the segment
    print(f'excluded_segments {excluded_segments}')    
    return excluded_segments

def format_speaker_number(speaker):
    # Format the speaker number with leading zeros
    speaker_number = int(speaker)
    formatted_speaker = f"SPEAKER_{speaker_number:02d}"
    return formatted_speaker

def remove_segments_from_audio(excluded_segments, original_audio_path, output_file):
    original_audio = AudioSegment.from_file(original_audio_path)

    for segment in excluded_segments:
        start_ms = int(segment['start'] * 1000)
        end_ms = int(segment['end'] * 1000)
        original_audio = original_audio[:start_ms] + original_audio[end_ms:]
    
    original_audio.export(output_file, format="wav")

def main():
    parser = argparse.ArgumentParser(description='Exclude desired speakers from final audio')
    parser.add_argument('--input_file', type=str, required=True, help="Path to the input directory containing audio files")
    parser.add_argument('--json_file', type=str, help='Path to the JSON file containing audio segments and speaker information')
    parser.add_argument('--excluded_speakers', type=str, help='List of speaker IDs to exclude')
    parser.add_argument('--output_file', type=str, default='final_audio.wav', help='Path to output final audio file')

    args = parser.parse_args()

    speakers_to_process = []
    if args.excluded_speakers:
        speakers_to_process = [format_speaker_number(speaker) for speaker in args.excluded_speakers.split(',')]

    excluded_segments = exclude_speakers(args.json_file, speakers_to_process)
    remove_segments_from_audio(excluded_segments, args.input_file, args.output_file)

if __name__ == "__main__":
    main()

# python "E:\example\tools\speaker_diarization_exclude.py" --input_file "C:\Users\Z820\Desktop\LSTQNNN_final\LSTQNNN1.wav" --json_file "C:\Users\Z820\Desktop\LSTQNNN_final\output\LSTQNNN1.wav-result_output.json" --output_file "C:\Users\Z820\Desktop\LSTQNNN_final\speaker_output\output.wav" --excluded_speakers 0,3

